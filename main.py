import unittest

class TestMethod(unittest.TestCase):

    def setUp(self):
        pass

    def test_z(self):
        self.assertEqual('z' * 3, 'zzz')

if __name__ == '__main__':
    unittest.main()